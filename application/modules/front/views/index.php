
    <body data-spy="scroll" data-target="#navmenu">
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
		<div class='preloader'><div class='loaded'>&nbsp;</div></div>
        <!--Home page style-->

        <header id="main_menu" class="header navbar-fixed-top">
            <div class="header_top_bar">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="nav">
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="main_menu_bg">
                <div class="container">
                    <div class="row">
                        <div class="nave_menu">
                            <nav class="navbar navbar-default" id="#navmenu">
                                <div class="container-fluid">
                                    <!-- Brand and toggle get grouped for better mobile display -->
                                    <div class="navbar-header">
                                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                            <span class="sr-only">Toggle navigation</span>
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                        </button>
                                        <a class="navbar-brand" href="#">
                                            <img src="assets/images/logo1.png"/>
                                        </a>
                                    </div>

                                    <!-- Collect the nav links, forms, and other content for toggling -->



                                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

                                        <ul class="nav navbar-nav navbar-right">
                                            <li><a href="#home">Beranda</a></li>
                                            <li><a href="#abouts">Cari Kosan</a></li>
                                            <li><a href="#feature">Login</a></li><!-- 
                                            <li><a href="#clients">Clients</a></li>
                                            <li><a href="#pricing">Domain</a></li>
                                            <li><a href="#contact">Hosting</a></li> -->

                                        </ul>
                                    </div>

                                </div>
                            </nav>
                        </div>  
                    </div>

                </div>

            </div>
        </header> <!--End of header -->



        <section id="home" class="home">
            <div class="home_overlay">
                <div class="container">
                    <div class="row">
                        <div class="main_slider_area">
                            <div class="slider">
                                <div class="single_slider">
                                    <h3>Nge-Kost</h3>
                                    <p>mudah  -  cepat  -  murah</p>
                                    <div class="single_slider_img_icon">
                                        <img src="assets/images/ii1.png" alt="" />
                                    </div>
                                    <div class="col-sm-6 col-sm-offset-3">
                                        <div class="home_searce_domain_area">
                                            <div class="main_home_searce_domain_content">
                                                <input type="text" placeholder="Masukan nama kosan/Daerah Kosan" />
                                                <!-- <select class="form-control">
                                                    <option>.com</option>
                                                    <option>.net</option>
                                                    <option>.co</option>
                                                </select> -->
                                                <input type="submit" value="Cari Kosan" />
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section><!-- End of Banner Section -->

        <section id="feature" class="feature">
            <div class="feature_overlay">
                <div class="container">
                    <div class="row">
                        <div class="main_feature_content_area">
                            <div class="col-sm-12">
                                <div class="heading_title text-center">
                                    <h2>Rekomendasi Kosan</h2>
                                    <p>Kota Bandung</p>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-sm-6">
                                    <a href="images/img_1.jpg" class="fh5co-card-item image-popup">
                                        <figure>
                                            <div class="overlay"><i class="ti-plus"></i></div>
                                            <img src="assets/images/img_1.jpg" alt="Image" class="img-responsive">
                                        </figure>
                                        <div class="fh5co-text">
                                            <h2>New York, USA</h2>
                                            <p>Far far away, behind the word mountains, far from the countries Vokalia..</p>
                                            <p><span class="btn btn-primary">Lebih Detail</span></p>
                                        </div>
                                    </a>
                                </div>

                                <div class="col-lg-4 col-md-4 col-sm-6">
                                    <a href="images/img_2.jpg" class="fh5co-card-item image-popup">
                                        <figure>
                                            <div class="overlay"><i class="ti-plus"></i></div>
                                            <img src="assets/images/img_2.jpg" alt="Image" class="img-responsive">
                                        </figure>
                                        <div class="fh5co-text">
                                            <h2>Seoul, South Korea</h2>
                                            <p>Far far away, behind the word mountains, far from the countries Vokalia..</p>
                                            <p><span class="btn btn-primary">Lebih Detail</span></p>
                                        </div>
                                    </a>
                                </div>

                                <div class="col-lg-4 col-md-4 col-sm-6">
                                    <a href="images/img_2.jpg" class="fh5co-card-item image-popup">
                                        <figure>
                                            <div class="overlay"><i class="ti-plus"></i></div>
                                            <img src="assets/images/img_2.jpg" alt="Image" class="img-responsive">
                                        </figure>
                                        <div class="fh5co-text">
                                            <h2>Seoul, South Korea</h2>
                                            <p>Far far away, behind the word mountains, far from the countries Vokalia..</p>
                                            <p><span class="btn btn-primary">Lebih Detail</span></p>
                                        </div>
                                    </a>
                                </div>

                                <div class="col-lg-4 col-md-4 col-sm-6">
                                    <a href="images/img_1.jpg" class="fh5co-card-item image-popup">
                                        <figure>
                                            <div class="overlay"><i class="ti-plus"></i></div>
                                            <img src="assets/images/img_1.jpg" alt="Image" class="img-responsive">
                                        </figure>
                                        <div class="fh5co-text">
                                            <h2>New York, USA</h2>
                                            <p>Far far away, behind the word mountains, far from the countries Vokalia..</p>
                                            <p><span class="btn btn-primary">Lebih Detail</span></p>
                                        </div>
                                    </a>
                                </div>

                                <div class="col-lg-4 col-md-4 col-sm-6">
                                    <a href="images/img_2.jpg" class="fh5co-card-item image-popup">
                                        <figure>
                                            <div class="overlay"><i class="ti-plus"></i></div>
                                            <img src="assets/images/img_2.jpg" alt="Image" class="img-responsive">
                                        </figure>
                                        <div class="fh5co-text">
                                            <h2>Seoul, South Korea</h2>
                                            <p>Far far away, behind the word mountains, far from the countries Vokalia..</p>
                                            <p><span class="btn btn-primary">Lebih Detail</span></p>
                                        </div>
                                    </a>
                                </div>

                                <div class="col-lg-4 col-md-4 col-sm-6">
                                    <a href="images/img_2.jpg" class="fh5co-card-item image-popup">
                                        <figure>
                                            <div class="overlay"><i class="ti-plus"></i></div>
                                            <img src="assets/images/img_2.jpg" alt="Image" class="img-responsive">
                                        </figure>
                                        <div class="fh5co-text">
                                            <h2>Seoul, South Korea</h2>
                                            <p>Far far away, behind the word mountains, far from the countries Vokalia..</p>
                                            <p><span class="btn btn-primary">Lebih Detail</span></p>
                                        </div>
                                    </a>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>


        <section id="clients" class="clients">
            <div class="container">
                <div class="row">
                    <div class="main_clients_area">

                        <div class="main_clients_top_content">
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="client_left_content">
                                        <h2>Our Clients</h2>
                                        <p>This is Photoshop's version  of Lorem Ipsum. 
                                            Proin gravida nibh vel velit auctor aliquet.
                                            Aenean sollicitudin, lorem quis bibendum.</p>
                                    </div>
                                </div>
                                <div class="col-sm-8">
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <div class="single_right_client">
                                                <img src="assets/images/c1.jpg" alt="" />
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="single_right_client">
                                                <img src="assets/images/c2.jpg" alt="" />
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="single_right_client">
                                                <img src="assets/images/c3.jpg" alt="" />
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="single_right_client">
                                                <img src="assets/images/c4.jpg" alt="" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="main_clients_bottom_content">
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="single_client_bottom">
                                        <p>“ This is Photoshop's version  of Lorem Ipsum. 
                                            Proin gravida nibh vel velit auctor aliquet. 
                                            Aenean sollicitudin, lorem quis bibendum auctor, 
                                            nisi elit consequat ipsum, nec sagittis sem nibh id elit. ”</p>
                                        <a href="">-Anna</a>
                                        <div class="singel_client_bottom_img">
                                            <img src="assets/images/cl1.jpg" alt="" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="single_client_bottom">
                                        <p>“ This is Photoshop's version  of Lorem Ipsum. 
                                            Proin gravida nibh vel velit auctor aliquet. 
                                            Aenean sollicitudin, lorem quis bibendum auctor, 
                                            nisi elit consequat ipsum, nec sagittis sem nibh id elit. ”</p>
                                        <a href="">-Anna</a>
                                        <div class="singel_client_bottom_img">
                                            <img src="assets/images/cl1.jpg" alt="" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="single_client_bottom">
                                        <p>“ This is Photoshop's version  of Lorem Ipsum. 
                                            Proin gravida nibh vel velit auctor aliquet. 
                                            Aenean sollicitudin, lorem quis bibendum auctor, 
                                            nisi elit consequat ipsum, nec sagittis sem nibh id elit. ”</p>
                                        <a href="">-Anna</a>
                                        <div class="singel_client_bottom_img">
                                            <img src="assets/images/cl1.jpg" alt="" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>



        <section id="pricing" class="pricing">
            <div class="container">
                <div class="row">
                    <div class="main_pricing"> 
                        <div class="col-sm-10 col-sm-offset-1">
                            <div class="row">
                                <div class="col-sm-8 col-xs-12">
                                    <div class="single_left_planig">
                                        <h2>Do you want to view Plans & Pricing ?</h2>
                                        <p>Follow the button in the right section.</p>
                                    </div>
                                </div>
                                <div class="col-sm-4 col-xs-12">
                                    <div class="single_right_planig">
                                        <a href="" class="btn btn-primary">Plans & Pricing</a> 
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section><!-- End of Pricing Section -->





        <section id="contact" class="contact">
            <div class="container">
                <div class="row">
                    <div class="main_contact">

                        <div class="contactwithmap_content">

                            <div class="contact_overlay">
                                <div class="col-sm-8 col-xs-12">
                                    <div class="locations_map">
                                        <img src="assets/images/mapbg.png" alt="" />
                                    </div>
                                </div>
                                <div class="col-sm-3 col-xs-12">
                                    <div class="contactwithmap_menu">
                                        <h2>Navigation</h2>
                                        <ul class="footer_menu">
                                            <li><a href="#home">Home</a></li>
                                            <li><a href="#abouts">About</a></li>
                                            <li><a href="#pricing">Pricing</a></li>
                                            <li><a href="#tours">Domain</a></li>
                                            <li><a href="#contact">Hosting</a></li>

                                        </ul>

                                        <div class="contactwithmap_socail_bookmark">
                                            <a href=""><i class="fa fa-twitter"></i></a>
                                            <a href=""><i class="fa fa-instagram"></i></a>
                                            <a href=""><i class="fa fa-google-plus"></i></a>
                                        </div>

                                    </div>


                                </div>                                
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </section><!-- End of contact With Map Section -->