<footer id="footer" class="footer">
            <div class="container">
                <div class="row">
                    <div class="main_footer text-center">
                        <p>Made with <i class="fa fa-heart"></i> by <a href="http://bootstrapthemes.co">Bootstrap Themes</a>2016. All Rights Reserved</p>
                    </div>
                </div>
            </div>
        </footer><!-- End of footer section -->

        <!-- STRAT SCROLL TO TOP -->

        <div class="scrollup">
            <a href="#"><i class="fa fa-chevron-up"></i></a>
        </div>

        <script src="<?php echo base_url('assets/front/js/vendor/jquery-1.11.2.min.js');?>"></script>
        <script src="<?php echo base_url('assets/front/js/vendor/bootstrap.min.js');?>"></script>
        <script src="<?php echo base_url('assets/front/js/vendor/isotope.min.js');?>"></script>

        <script src="<?php echo base_url('assets/front/js/jquery.easypiechart.min.js');?>"></script>
        <script src="<?php echo base_url('assets/front/js/jquery.mixitup.min.js');?>"></script>
        <script src="<?php echo base_url('assets/front/js/jquery.easing.1.3.js');?>"></script>
        <script src="http://maps.google.com/maps/api/js"></script>
        <script src="<?php echo base_url('assets/front/js/gmaps.min.js');?>"></script>
        <script>
            var map = new GMaps({
                el: '.map',
                scrollwheel: false,
                lat: -12.043333,
                lng: -77.028333
            });
        </script>

        <script src="<?php echo base_url('assets/front/js/plugins.js');?>"></script>
        <script src="<?php echo base_url('assets/front/js/main.js');?>"></script>

    </body>
</html>