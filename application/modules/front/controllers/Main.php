<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {

	public function index()
	{
		$title['title'] = 'Home';

		$this->load->view('front_header', $title);
		$this->load->view('index');
		$this->load->view('front_footer');
	}
}
