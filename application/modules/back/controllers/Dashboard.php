<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	function index() {
		// $this->load->view('back/header');
		$this->load->view('index');
		// $this->load->view('back/footer');
	}

	function map() {
		// $this->load->view('back/header');
		$this->load->view('map');
		// $this->load->view('back/footer');
	}
}