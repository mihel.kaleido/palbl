<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_dashboard extends CI_Model {

	// METHOD TAMBAH BERITA
	function insert_berita($data){
		$this->db->insert('tb_berita', $data);
	}

	// METHOD TAMPIL BERITA
	function view_berita() {
		$this->db->select('*');
		$this->db->from('tb_berita');

		return $this->db->get();
	}

	// METHOD TAMBAH ACARA
	function insert_acara($data){
		$this->db->insert('tb_acara', $data);
	}

	// METHOD TAMPIL ACARA
	function view_acara() {
		$this->db->select('*');
		$this->db->from('tb_acara');

		return $this->db->get();
	}
}