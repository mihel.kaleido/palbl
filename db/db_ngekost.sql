-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 05, 2018 at 01:04 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.1.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_ngekost`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_detailuser`
--

CREATE TABLE `tbl_detailuser` (
  `id_detailUser` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `umur` int(3) NOT NULL,
  `no_ktp` varchar(16) DEFAULT NULL,
  `no_telepon` varchar(15) NOT NULL,
  `alamat` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_detailuser`
--

INSERT INTO `tbl_detailuser` (`id_detailUser`, `nama`, `umur`, `no_ktp`, `no_telepon`, `alamat`) VALUES
(1, 'hello world', 22, NULL, '098787987', 'laptop'),
(2, 'helmi mutawalli', 20, NULL, '097099897', 'gerlong');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_fasilitas`
--

CREATE TABLE `tbl_fasilitas` (
  `id_fasilitas` int(11) NOT NULL,
  `dapur` tinyint(1) NOT NULL,
  `kulkas` tinyint(1) NOT NULL,
  `AC` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_foto`
--

CREATE TABLE `tbl_foto` (
  `id_foto` int(11) NOT NULL,
  `nama_foto` int(11) NOT NULL,
  `alamat_foto` int(11) NOT NULL,
  `ukuran_foto` int(11) NOT NULL,
  `ekstensi_foto` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_kosan`
--

CREATE TABLE `tbl_kosan` (
  `id_kosan` int(11) NOT NULL,
  `id_pemlik` int(11) NOT NULL,
  `nama_kosan` varchar(100) NOT NULL,
  `fasilitas` int(11) NOT NULL,
  `luas_kamar` int(11) NOT NULL,
  `keterangan` varchar(1000) NOT NULL,
  `foto` int(11) NOT NULL,
  `kamar_kosong` int(2) NOT NULL,
  `harga` int(11) NOT NULL,
  `lokasi` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_lokasi`
--

CREATE TABLE `tbl_lokasi` (
  `id_lokasi` int(11) NOT NULL,
  `longitude` int(11) NOT NULL,
  `latitude` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_luaskamar`
--

CREATE TABLE `tbl_luaskamar` (
  `id_luaskamar` int(11) NOT NULL,
  `lebar` int(11) NOT NULL,
  `panjang` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `id_user` int(11) NOT NULL,
  `nama` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(50) NOT NULL,
  `level` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`id_user`, `nama`, `username`, `password`, `level`) VALUES
(1, 2, 'helmi', 'qwe', 'admin'),
(2, 1, 'hello', 'hello', 'pengunjung');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_detailuser`
--
ALTER TABLE `tbl_detailuser`
  ADD PRIMARY KEY (`id_detailUser`);

--
-- Indexes for table `tbl_fasilitas`
--
ALTER TABLE `tbl_fasilitas`
  ADD PRIMARY KEY (`id_fasilitas`);

--
-- Indexes for table `tbl_foto`
--
ALTER TABLE `tbl_foto`
  ADD PRIMARY KEY (`id_foto`),
  ADD KEY `nama_foto` (`nama_foto`);

--
-- Indexes for table `tbl_kosan`
--
ALTER TABLE `tbl_kosan`
  ADD PRIMARY KEY (`id_kosan`),
  ADD KEY `id_pemlik` (`id_pemlik`),
  ADD KEY `fasilitas` (`fasilitas`),
  ADD KEY `luas_kamar` (`luas_kamar`),
  ADD KEY `foto` (`foto`),
  ADD KEY `lokasi` (`lokasi`);

--
-- Indexes for table `tbl_lokasi`
--
ALTER TABLE `tbl_lokasi`
  ADD PRIMARY KEY (`id_lokasi`);

--
-- Indexes for table `tbl_luaskamar`
--
ALTER TABLE `tbl_luaskamar`
  ADD PRIMARY KEY (`id_luaskamar`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`id_user`),
  ADD KEY `nama` (`nama`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_detailuser`
--
ALTER TABLE `tbl_detailuser`
  MODIFY `id_detailUser` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tbl_fasilitas`
--
ALTER TABLE `tbl_fasilitas`
  MODIFY `id_fasilitas` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_foto`
--
ALTER TABLE `tbl_foto`
  MODIFY `id_foto` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_kosan`
--
ALTER TABLE `tbl_kosan`
  MODIFY `id_kosan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tbl_lokasi`
--
ALTER TABLE `tbl_lokasi`
  MODIFY `id_lokasi` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_luaskamar`
--
ALTER TABLE `tbl_luaskamar`
  MODIFY `id_luaskamar` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `tbl_detailuser`
--
ALTER TABLE `tbl_detailuser`
  ADD CONSTRAINT `tbl_detailuser_ibfk_1` FOREIGN KEY (`id_detailUser`) REFERENCES `tbl_user` (`nama`) ON DELETE CASCADE;

--
-- Constraints for table `tbl_kosan`
--
ALTER TABLE `tbl_kosan`
  ADD CONSTRAINT `tbl_kosan_ibfk_1` FOREIGN KEY (`id_pemlik`) REFERENCES `tbl_detailuser` (`id_detailUser`) ON DELETE CASCADE,
  ADD CONSTRAINT `tbl_kosan_ibfk_2` FOREIGN KEY (`fasilitas`) REFERENCES `tbl_fasilitas` (`id_fasilitas`),
  ADD CONSTRAINT `tbl_kosan_ibfk_3` FOREIGN KEY (`luas_kamar`) REFERENCES `tbl_luaskamar` (`id_luaskamar`),
  ADD CONSTRAINT `tbl_kosan_ibfk_4` FOREIGN KEY (`lokasi`) REFERENCES `tbl_lokasi` (`id_lokasi`),
  ADD CONSTRAINT `tbl_kosan_ibfk_5` FOREIGN KEY (`foto`) REFERENCES `tbl_foto` (`nama_foto`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
